from rest_framework import viewsets

from blog.models import FormularioUserPerris
from api.serializers import FormularioUserPerrisSerializer,UserSerializer,GCMCustomSerializer
from django.shortcuts import get_object_or_404

from django.contrib.auth.models import User, Group
from push_notifications.models import GCMDevice


class GCMCustomViewSet(viewsets.ModelViewSet):
    queryset = GCMDevice.objects.all().order_by('id')
    serializer_class = GCMCustomSerializer

class FormulariosUserViewSet(viewsets.ModelViewSet):
    queryset = FormularioUserPerris.objects.all()
    serializer_class = FormularioUserPerrisSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer