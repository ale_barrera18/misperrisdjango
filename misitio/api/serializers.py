from django.contrib.auth.models import User

from blog.models import FormularioUserPerris
from rest_framework import serializers
from push_notifications.models import GCMDevice
class GCMCustomSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GCMDevice
        fields = ('url', 'id', 'registration_id')

    def validate_registration_id(self, value):
        validate_device = GCMDevice.objects.filter(registration_id=value)
        if(validate_device):
            raise serializers.ValidationError("register_id ya existe")
        return value

    def create(self, validated_data):
        device=GCMDevice.objects.create(registration_id=validated_data.pop('registration_id'),
                                          active=True, cloud_message_type="FCM")
        device.save()
        return device

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username','email', 'password')

    

class FormularioUserPerrisSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FormularioUserPerris
        fields= ('url','email','rut','nombre','segundoNombre','apellidoPaterno','apellidoMaterno','fechaNacimiento','fono', 'region', 'comuna', 'tipoCasa','tipo','password_1')
    '''
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({"Success": "Data posted successfully"},status=status.HTTP_201_CREATED, headers=headers)'''