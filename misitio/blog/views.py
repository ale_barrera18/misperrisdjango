from django.shortcuts import render,render_to_response, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import FormularioUserPerris, MascotasPerro
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.urls import reverse

# Create your views here.
# Create your views here.
def index(request):

    
    try:
        nombre = request.session['nombre']
        apellidoPaterno = request.session['apellidoPaterno']
        tipo = request.session['tipo']
        mascotas = MascotasPerro.objects.all()
        
        return render(request, 'blog/index.html', {'nombre':nombre,
                                                   'apellidoPaterno': apellidoPaterno,
                                                   'tipo':tipo,
                                                   'mascotas':mascotas})
    except:
        mascotas = MascotasPerro.objects.all()
        nombre = ""
        return render(request, 'blog/index.html', {'mascotas':mascotas,
                                                    'nombre': nombre})
        

        
    
    
    

def menu_admin(request):
    return render(request, 'blog/menu_admin.html', {})


def agregarPerro(request):

    if request.method == 'POST':
        #request.files = para obtener el archivo, averiguar
        mensaje = ""
        error = ""
        
        try:
            mascota = MascotasPerro()

            file = request.FILES['file']
            #ext = file.name.split(".")[-1]
            #file.name = str(mascota.pk) + "." + ext
            mascota.file = file
            mascota.save()
            mascota.nombrePerro = request.POST['nombre']
            
            mascota.razaPredominante = request.POST['raza']
            
            mascota.descripcion = request.POST['descripcion']
            
            mascota.estado = request.POST['estado']
            
            mascota.save()

            mensaje = "Animal almacenado correctamente"

            return render(request, 'mantenedor/agregarPerro.html', {'mensaje':mensaje,
                                                                    'nombre':request.session['nombre'],
                                                                    'apellidoPaterno':request.session['apellidoPaterno']})

        except Exception as e:
            error = "Error al guardar el perfil del perro: " + str(e)
            return render(request, 'mantenedor/agregarPerro.html', {'error': error,
                                                                    'nombre':request.session['nombre'],
                                                                    'apellidoPaterno':request.session['apellidoPaterno']})

        #a = Mascota.objects.create(fotografia = fotografia, nombrePerro = nombre, razaPredominante = raza, descripcion = descripcion, estado = estado)
        #a.save()
        
    else:
        return render(request, 'mantenedor/agregarPerro.html',{
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})
def listarPerro(request):

    mascotas = MascotasPerro.objects.all()

    return render(request, 'mantenedor/listarPerro.html',{'mascotas': mascotas,
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})



def eliminarPerro(request, id):
    mascota = MascotasPerro.objects.get(id=id)

    try:
        mascota.delete()
        mensaje = "Animal fue eliminado Correctamente"
        messages.success(request, mensaje)
        return render(request,'blog:listaPerro',{'nombre':request.session['nombre'],
                                                             'apellido':request.session})
    except:
        
        return redirect('blog:listarPerro')
        
def modificarPerro(request,id):
    mascota = MascotasPerro.objects.get(id = id)
    if request.method == 'POST':
        #request.files = para obtener el archivo, averiguar
        mensaje = ""
        error = ""
        
        try:
            mascota = MascotasPerro()
            mascota.id = request.POST.get('txtId')
            file = request.FILES['file']
            

            


            mascota.file = file
            
            mascota.nombrePerro = request.POST['nombre']
            
            mascota.razaPredominante = request.POST['raza']
            
            mascota.descripcion = request.POST['descripcion']
            
            mascota.estado = request.POST['estado']
            
            mascota.save()

            mensaje = "Animal modificado correctamente"
            messages.success(request, mensaje)
            return redirect('blog:listarPerro')
            

        except Exception as e:
            mensaje = "Error al modificar el perfil del perro: " + str(e)
            messages.error(request, mensaje)
        return redirect('blog:listarPerro')
    
    return render(request, 'mantenedor/modificarPerro.html', {'mascota':mascota,
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})
    
def disponible(request):
    a = "Disponible"
    mascota = MascotasPerro.objects.filter(estado = a)
    return render(request, 'mantenedor/listarPerro.html',{'mascotas':mascota,
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})

def adoptado(request):
    a = "Adoptado"
    mascota = MascotasPerro.objects.filter(estado = a)
    return render(request, 'mantenedor/listarPerro.html',{'mascotas':mascota,
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})

def rescatado(request):
    a = "Rescatado"
    mascota = MascotasPerro.objects.filter(estado = a)
    return render(request, 'mantenedor/listarPerro.html',{'mascotas':mascota,
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})

def todos(request):
    mascota = MascotasPerro.objects.all()
    return render(request, 'mantenedor/listarPerro.html',{'mascotas':mascota,
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']} )

def disponibleCliente(request):
    a = "Disponible"
    mascota = MascotasPerro.objects.filter(estado = a)
    return render(request, 'blog/mantenedor.html',{'mascotas':mascota,
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})

def adoptadoCliente(request):
    a = "Adoptado"
    mascota = MascotasPerro.objects.filter(estado = a)
    return render(request, 'blog/mantenedor.html',{'mascotas':mascota,
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})

def rescatadoCliente(request):
    a = "Rescatado"
    mascota = MascotasPerro.objects.filter(estado = a)
    return render(request, 'blog/mantenedor.html',{'mascotas':mascota,
                                                          'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})

def filtroDisponibleIndex(request):
    a = "Disponible"
    mascotas = MascotasPerro.objects.filter(estado=a)

    try:
        return render(request, 'blog/index.html', {'disponibles':mascotas,
                                                'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})
        
    except:
        return render(request, 'blog/index.html', {'disponibles':mascotas,
                                                    'nombre':""})
    
    


def filtroRescatadoIndex(request):
    a = "Rescatado"
    mascotas = MascotasPerro.objects.filter(estado=a)
    try:
        return render(request, 'blog/index.html', {'rescatados':mascotas,
                                                'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})
        
    except:
        return render(request, 'blog/index.html', {'rescatados':mascotas})
    

def filtroAdoptadoIndex(request):
    a = "Adoptado"
    mascotas = MascotasPerro.objects.filter(estado=a)
    try:
        return render(request, 'blog/index.html', {'adoptados':mascotas,
                                                'nombre':request.session['nombre'],
                                                          'apellidoPaterno':request.session['apellidoPaterno']})
        
    except:
        return render(request, 'blog/index.html', {'adoptados':mascotas})

def formulario(request):
    if request.method =='POST':
        try:
            email = request.POST['email']
            rut = request.POST['username']
            password_1 = request.POST['password']


            b = User.objects.create_user(username =rut, password= password_1, email=email)
            b.save()

            a = User.objects.get(username = rut)
            request.session['idUsuario'] = a.id
            request.session['email'] = a.email
            request.session['password'] = a.password
            request.session['tipo'] = "Cliente"
            return render(request,'blog/formulario2.html',{'idUsuario':request.session['idUsuario'],
                                                            'email':request.session['email'],
                                                            'password' :request.session['password'],
                                                            'tipo':request.session['tipo']})
        except:
            return render(request, 'blog/formulario.html', {})
    
        

    else:
        return render(request, 'blog/formulario.html', {})







    
    

def solicitudPerro(request,id):
    mascota = MascotasPerro.objects.get(id = id)
    mascota.estado = "Adoptado"
    mascota.save()

    return render(request, 'mantenedor/solicitud.html',{'nombre': request.session['nombre'],
                                                            'apellidoPaterno':request.session['apellidoPaterno']})

def validador2(request,id):
    
    
    try:
        mascota = MascotasPerro.objects.get(id = id)
        mascota.estado = "Adoptado"
        mascota.save()
        return render(request, 'mantenedor/solicitud.html',{'nombre': request.session['nombre'],
                                                            'apellidoPaterno':request.session['apellidoPaterno']})
        
    except:
        error = "No se puede adoptar mascota, ya que no posee una cuenta"
        return render(request, 'blog/index.html',{'nombre': request.session['nombre'],
                                                            'apellidoPaterno':request.session['apellidoPaterno'],
                                                            'errorSolicitud':error} )


def inicioSesion(request):
    context = {}
    

    if request.method == 'POST':

        

        try:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                # Para agregar variables en session debe escribir la siguiente línea:
                # request.session['nomre_variable'] = valor_variable
                login(request, user)
                
                
                
                return HttpResponseRedirect(reverse('blog:usuario_success'))
                #return HttpResponseRedirect(reverse('blog:'))
            else:
                context = {'error': "Usuario y/o contraseña incorrectas"}
                return render(request, 'registration/login.html', context)

                
        except:
            context = {'error': "Error en la página"}
            return render(request, 'registration/login.html', context)

        
            
    else:

        
        return render(request,"registration/login.html",context)



def success(request):
    if request.user.is_authenticated:
        user = request.user

        usuario = FormularioUserPerris.objects.get(rut__exact = user)
        
        request.session['nombre']=usuario.nombre
        request.session['apellidoPaterno']=usuario.apellidoPaterno
        request.session['tipo']=usuario.tipo
       
        
        
        if usuario.tipo == "Cliente":
            a = 'Disponible'
            
            
            #listar, averiguar
            mascotas = MascotasPerro.objects.filter(estado=a)
            return render(request, 'blog/mantenedor.html',{'cuenta':usuario.rut,
                                                            'nombre': request.session['nombre'],
                                                            'apellidoPaterno':request.session['apellidoPaterno'],
                                                            'mascotas':mascotas})
            
                                                                            
        else:
            return render(request,'blog/menu_admin.html',{'cuenta':usuario.rut,
                                                            'nombre': request.session['nombre'],
                                                            'apellidoPaterno':request.session['apellidoPaterno']})
    else:
        return HttpResponseRedirect(reverse('blog:inicioSesion'))
    
            

def cerrarSeccion(request):
    if request.method == 'POST':
        
        del request.session['nombre']
        del request.session['apellidoPaterno']
        del request.session['tipo']
        logout(request)
        
        
        
        return HttpResponseRedirect(reverse('blog:inicioSesion'))
    
    



        

   
    
    