$(function () {
    
    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
      }, "Solo letras");
  
    $("#formulario").validate(
        {
            rules: {
                email:{
                    required: true,
                    email: true
                },
                run: {
                    required: true,
                },
                nombre: {
                    required: true,
                    lettersonly: true
                },
                segundoNombre:{
                    required: true,
                    lettersonly: true
                },
                apellidoPaterno:{
                    required: true,
                    lettersonly: true
                },
                apellidoMaterno:{
                    required: true,
                    lettersonly: true
                },
                fch_nac: {
                    required: true,
                },
                fono: {
                    required: true,
                    digits: true
                },
                vivienda:{
                    required: true,
                    min: 1
                },
                pw1:{
                    required: true,
                    minlength: 4,
                },
                pw2:{
                    required: true,
                    minlength: 4,
                    equalTo : "#pw1"
                }         
            },
            messages: {
                email:{
                    required: 'Correo Electrónico es obligatorio',
                    email: 'Ingrese formato valido'
                },
                run: {
                    required: 'Run es obligatorio'
                },
                nombre: {
                    required: 'Nombre Completo es obligatorio',
                    lettersonly: 'Solo se permiten letras'
                },
                segundoNombre: {
                    required: 'Nombre Completo es obligatorio',
                    lettersonly: 'Solo se permiten letras'
                },
                apellidoPaterno: {
                    required: 'Nombre Completo es obligatorio',
                    lettersonly: 'Solo se permiten letras'
                },
                apellidoMaterno: {
                    required: 'Nombre Completo es obligatorio',
                    lettersonly: 'Solo se permiten letras'
                },
                fch_nac: {
                    required: 'Fecha de nacimiento es obligatoria',
                },
                fono: {
                    required: 'Fono es obligatorio',
                    digits: 'Solo se permiten numeros'
                },
                vivienda: {
                    required: 'La selección de una vivienda es obligatoria',
                    min: 'Debe seleccionar una vivenda de la lista'
                },  
                pw1:{
                    required: 'La contraseña es obligatoria',
                    minlength: 'El minimo de su contraseña debe ser de 4 caracteres'
                },
                pw2:{
                    required: 'La confirmacion de contraseña es obligatoria',
                    minlength: 'El minimo de su contraseña debe ser de 4 caracteres',
                    equalTo: 'La contraseña NO es igual'
                }
            },
        }
    )

});

$(function(){
    $("#btn_campana").click(function(){
        $("#confirmacion").dialog({
            title: "Formulario Enviado",
            width: 400,
            heiht: 300,
            model: true,
            buttons: {
                close:
                function(){
                    $(this).dialog("close")
                }
            }

        })
    });
});

