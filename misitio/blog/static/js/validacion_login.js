$(function () {

    $("#login").validate(
        {
            rules: {
                username:{
                    required: true
                },
                password:{
                    required: true,
                    minlength: 4,
                }
            },
            messages: {
                username:{
                    required: 'Debe ingresar su RUT de usuario'
                },
                password:{
                    required: 'Debe ingresar una contraseña para iniciar sesion',
                    minlength: 'El largo minimo de una contraseña es de 4 caracteres'
                }
            },
        }
    )

});

$(function(){
    $("#btn_campana").click(function(){
        $("#confirmacion").dialog({
            title: "Formulario Enviado",
            width: 400,
            heiht: 300,
            model: true,
            buttons: {
                close:
                function(){
                    $(this).dialog("close")
                }
            }

        })
    });
});

