$(document).ready(
    function(){

        $.getJSON("https://apis.modernizacion.cl/dpa/regiones", function(result){
            
            
            var htmlRegion = '<option value="0">Seleccione región</option>';
            $.each(result, function(index, item){
                //console.log(item.nombre);
                htmlRegion = htmlRegion + '<option value="' + item.codigo +'">'+ item.nombre + '</option>';
                //console.log(htmlRegion);
            });
            $("#region").html(htmlRegion);
        });
    }
);

$(document).on("change", "#region", function(event){
    var codRegion = $(this).val();
    //console.log(codRegion);

    $.getJSON("https://apis.modernizacion.cl/dpa/regiones/" + codRegion + "/comunas", function(result){
        var ivalue = 1;
        var htmlComuna = '<option value="0">Seleccione Comuna</option>';

        $.each(result, function(index, item){
            //console.log(item.nombre);
            htmlComuna = htmlComuna + '<option value="' + ivalue +'">'+ item.nombre + '</option>';
            ivalue++;
        });
        $("#comuna").html(htmlComuna);
    });

})
