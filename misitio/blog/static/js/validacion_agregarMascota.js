$(function () {
    
    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
      }, "Solo letras");
  
    $("#menu_agregarMascota").validate(
        {
            rules: {
                nombre:{
                    required: true,
                },
                raza:{
                    required: true,
                },
                descripcion:{
                    required: true,
                },
            },
            messages: {
                nombre:{
                    required: 'Nombre de la mascota requerido',
                },
                raza:{
                    required: 'Raza de la mascota requerido',
                },
                descripcion:{
                    required: 'Descripcion de la mascota requerido',
                }
               
            },
        }
    )

});


