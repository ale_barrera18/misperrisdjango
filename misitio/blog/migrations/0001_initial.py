# Generated by Django 2.1.2 on 2018-10-31 19:55

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Formulario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('nombre', models.CharField(max_length=50)),
                ('segundoNombre', models.CharField(max_length=50)),
                ('apellidoPaterno', models.CharField(max_length=50)),
                ('apellidoMaterno', models.CharField(max_length=50)),
                ('fechaNacimiento', models.DateField()),
                ('fono', models.CharField(max_length=12)),
                ('region', models.CharField(max_length=90)),
                ('comuna', models.CharField(max_length=90)),
                ('tipoCasa', models.CharField(max_length=30)),
                ('tipo', models.CharField(max_length=30)),
                ('password_1', models.CharField(max_length=30)),
                ('rut', models.ForeignKey(on_delete='CASCADE', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
