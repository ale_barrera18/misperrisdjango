# Generated by Django 2.1.2 on 2018-11-29 16:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0014_formulariouserperris'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formulariouserperris',
            name='rut',
        ),
        migrations.DeleteModel(
            name='MascotasPerro',
        ),
        migrations.DeleteModel(
            name='FormularioUserPerris',
        ),
    ]
