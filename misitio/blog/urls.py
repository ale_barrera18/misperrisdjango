from django.urls import path, include
from django.conf.urls import url, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

from django.contrib.auth import views as auth_views


app_name = 'blog'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('formulario', views.formulario, name ='formulario'),
    
    path('inicioSesion', views.inicioSesion , name='inicioSesion' ),
    path('success',views.success, name = 'usuario_success'),
    path('cerrarSeccion',views.cerrarSeccion, name = 'cerrarSeccion'),

    path('menu_admin', views.menu_admin , name='menu_admin' ),
    path('agregarPerro', views.agregarPerro , name='agregarPerro' ),
    path('listarPerro', views.listarPerro , name='listarPerro' ),
   
    path('eliminarPerro/<id>', views.eliminarPerro , name='eliminarPerro' ),
    path('modificarPerro/<id>', views.modificarPerro , name='modificarPerro' ),
    path('solicitudPerro/<id>', views.solicitudPerro , name='solicitudPerro' ),

    path('disponible', views.disponible, name='disponible'),
    path('rescatado', views.rescatado, name='rescatado'),
    path('adoptado', views.adoptado, name='adoptado'),
    path('todos', views.todos, name ="todos"),

    path('filtroDisponibleIndex', views.filtroDisponibleIndex, name = 'filtroDisponibleIndex'),
    path('filtroRescatadoIndex', views.filtroRescatadoIndex, name = 'filtroRescatadoIndex'),
    path('filtroAdoptadoIndex', views.filtroAdoptadoIndex, name = 'filtroAdoptadoIndex'),
    path('validador2/<id>', views.validador2, name = "validador2"),

    #path('password-reset',auth_views.PasswordResetView.as_view(template_name='registration/password_reset.html'),name='password_reset'),
    #path('password-reset/done',auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_done.html'),name='password_reset_done'),
    #path('password-reset-confirm/<uidb64>/<token>',auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm.html'),name='password_reset_confirm'),
    url('', include('pwa.urls')),
    url(r'^password_reset/$', auth_views.PasswordResetView, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.PasswordResetDoneView,name= 'password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',auth_views.PasswordResetConfirmView,name='password_reset_confirm'),
    url(r'^reset/done', auth_views.PasswordResetCompleteView, name = 'password_reset_complete'),
    #path('solicitudPerro/<id>', views.solicitudPerro , name='solicitudPerro' ),

    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)