from django.db import models

# Create your models here.
from django.utils import timezone
from django.contrib.auth.models import User
# Create your models here.

class FormularioUserPerris(models.Model):
    email = models.EmailField()
    rut = models.OneToOneField(User,on_delete='models.CASCADE')
    nombre = models.CharField(max_length=50)
    segundoNombre = models.CharField(max_length=50)
    apellidoPaterno = models.CharField(max_length=50)
    apellidoMaterno = models.CharField(max_length=50)
    fechaNacimiento = models.DateField()
    fono = models.CharField(max_length=12)
    region = models.CharField(max_length=90)
    comuna = models.CharField(max_length=90)
    tipoCasa = models.CharField(max_length=30)
    tipo = models.CharField(max_length = 30)
    password_1 = models.CharField(max_length =250)  

    def create(self):
        self.save() 

    def __str__(self):
        return self.nombre + " " + self.apellidoPaterno + " " + self.apellidoMaterno

    


    

class MascotasPerro(models.Model):
    
    file = models.FileField(blank=True, null=True, upload_to="blog")
    nombrePerro = models.CharField(max_length = 30)
    razaPredominante = models.CharField(max_length = 50)
    descripcion = models.TextField()
    estado = models.CharField(max_length = 50)

    def __str__(self):
        return self.nombrePerro + " - "+ self.estado
    
    def create(self):
        self.save()

    




    
