importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js'); 
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebasemessaging.js'); 
importScripts('firebaseinit.js'); 

var messaging = firebase.messaging(); 
messaging.setBackgroundMessageHandler(function(payload) {   
    console.log('[firebase-messaging-sw.js] Received background message ', payload);   
      
    
    var notificationTitle = 'Background Message Title';   
    var notificationOptions = {     
        body: 'Background Message body.',     
        icon: '/firebase-logo.png'   
    }; 
 
    return self.registration.showNotification(notificationTitle, notificationOptions);

});

navigator.serviceWorker.register('./sw.js').then((registration) => 
{   messaging.useServiceWorker(registration); 

    const messaging = firebase.messaging();


    messaging.usePublicVapidKey("BNTORAgdHs8dTs3D4LFAgk_ZG5zS1HcAueYaWsJ75QEBc0SZQtBecsAuunpBx6neL6MqzwuaIJRTX2qKV3EMGaQ");

    messaging.requestPermission().then(function () {
        console.log('Notification permission granted.');
    }).catch(function (err) {
        console.log('Unable to get permission to notify.', err);
    });

    messaging.getToken().then(function (currentToken) {
        if (currentToken) {
            console.log('Token generated.');

            console.log(currentToken)
            //Generamos nuestra función para enviar el token al servidor
            sendTokenToServer(currentToken);
        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI.
            setTokenSentToServer(false);
        }
    }).catch(function (err) {
        console.log('An error occurred while retrieving token. ', err);
        setTokenSentToServer(false);
    });

    messaging.onTokenRefresh(function () {
        messaging.getToken().then(function (refreshedToken) {
            console.log('Token refreshed.');

            console.log(refreshedToken)
            // Indicate that the new Instance ID token has not yet been sent to the
            // app server.
            setTokenSentToServer(false);
            sendTokenToServer(refreshedToken);
            // Send Instance ID token to app server.
            //Generamos nuestra función para enviar el token al servidor
        }).catch(function (err) {
            console.log('Unable to retrieve refreshed token ', err);
        });


        messaging.onMessage(function (payload) {
            console.log('Message received. ', payload);
        });

        function setTokenSentToServer(sent) {
            window.localStorage.setItem('sentToServer', sent ? 1 : 0);
        }

        function isTokenSentToServer() {
            return window.localStorage.getItem('sentToServer') === 1;
        }

    });

});
 
  